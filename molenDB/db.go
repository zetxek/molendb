package db

import (
	"database/sql"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/zetxek/molendb/models"
	_ "github.com/mattn/go-sqlite3"
)

// GetDBName provides the db name (and location)
func GetDBName() string {
	return "./mills.db"
}

// OpenDB returns a connection to the wished db
func OpenDB(name string) *sql.DB {
	db, err := sql.Open("sqlite3", name)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

// PopulateDB inserts data in the given db, with the xml in the given directory
func PopulateDB(dir string, db *sql.DB) {
	files, _ := ioutil.ReadDir(dir)

	for _, f := range files {
		fmt.Println(f.Name())
		mills := parseFile(fmt.Sprintf("./files/%s", f.Name()))
		for _, mill := range mills {
			Insert(db, mill)
		}
	}
}

func parseFile(file string) []models.Mill {
	xmlFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Error opening file:", err)
	}
	defer xmlFile.Close()

	var x models.XMLMills
	data, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println("Error unmarshalling: ", err)
	} else {
		xml.Unmarshal(data, &x)
		return x.Mills
	}

	return []models.Mill{}
}

func Createdb() {
	os.Remove("./mills.db")

	db, err := sql.Open("sqlite3", "./mills.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
	create table mills (id integer not null primary key, name text, address text, lat float, lng float, type text, photo text);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}

}

// Insert creates a new record in the given db
func Insert(db *sql.DB, m models.Mill) {

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into mills(id, name, address, lat, lng, type, photo) values(?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(m.Number, m.Name, m.Address, m.Lat, m.Lng, m.MillType, m.Photo)
	if err != nil {
		log.Fatal(err)
	}

	tx.Commit()

}

// ClosestMill prints the closest windmill to a given location, as a pair of lat,lng
func CloseMills(db *sql.DB, lat float32, lng float32) []models.SearchResult {

	stmt, err := db.Prepare("SELECT *, (((lat-?)*(lat-?)) + ((lng - ?)*(lng - ?)) ) *10000  as distance FROM mills ORDER BY distance ASC limit 1")
	if err != nil {
		log.Fatal(err)
	}

	rows, err := stmt.Query(lat, lat, lng, lng)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var res []models.SearchResult
	for rows.Next() {
		var id int
		var name string
		var address string
		var mlat float32
		var mlng float32
		var millType string
		var photo string
		var distance float32
		err = rows.Scan(&id, &name, &address, &mlat, &mlng, &millType, &photo, &distance)
		if err != nil {
			log.Fatal(err)
		}
		var m = models.Mill{Name: name, Address: address, Lat: mlat, Lng: mlng, MillType: millType, Photo: photo}
		var sr = models.SearchResult{Mill: m, Distance: distance}
		res = append(res, sr)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return res
}

func ClosestMill(db *sql.DB, lat float32, lng float32) models.SearchResult {
	var ws = CloseMills(db, lat, lng)
	return ws[0]
}

// ListItems prints all the items in the db
func ListItems(db *sql.DB) {

	rows, err := db.Query("select * from mills")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		var name string
		var address string
		var lat float32
		var lng float32
		var millType string
		var photo string
		err = rows.Scan(&id, &name, &address, &lat, &lng, &millType, &photo)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, name)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
}
