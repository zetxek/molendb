package models

type SearchResult struct {
	Mill     Mill
	Distance float32
}
