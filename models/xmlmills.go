package models

import "encoding/xml"

// XMLMills is used to parse the xml
type XMLMills struct {
	XMLName xml.Name `xml:"markers"`
	Mills   []Mill   `xml:"marker"`
}
