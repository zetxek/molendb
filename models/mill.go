package models

import "encoding/xml"

// Mill is the basic data managed by the application (as a windmill)
type Mill struct {
	XMLName  xml.Name `xml:"marker"`
	Number   string   `xml:"nummer,attr"`
	Name     string   `xml:"name,attr"`
	Address  string   `xml:"address,attr"`
	Lat      float32  `xml:"lat,attr"`
	Lng      float32  `xml:"lng,attr"`
	MillType string   `xml:"type,attr"`
	Photo    string   `xml:"foto1,attr"`
}
