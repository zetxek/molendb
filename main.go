package main

import (
	"bitbucket.org/zetxek/molendb/molenDB"
	"fmt"
)

func main() {

	d := db.OpenDB(db.GetDBName())
	defer d.Close()

	// Dam Square
	var m = db.ClosestMill(d, 52.3759976, 4.8264306)
	fmt.Printf("Closest mill is: %+v", m)
}
